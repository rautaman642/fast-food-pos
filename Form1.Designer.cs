﻿namespace fastfood
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.Closelbl = new System.Windows.Forms.Label();
            this.Datelbl = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CheeseTb = new System.Windows.Forms.TextBox();
            this.ChickenTb = new System.Windows.Forms.TextBox();
            this.SandwichTb = new System.Windows.Forms.TextBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.CheeseCb = new System.Windows.Forms.CheckBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.ChickenCb = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SandwichCb = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SaladTb = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.SaladCb = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.BurgerTb = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.BurgerCb = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.FriesTb = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.FriesCb = new System.Windows.Forms.CheckBox();
            this.friesecb = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.orangeTb = new System.Windows.Forms.TextBox();
            this.pancakeTb = new System.Windows.Forms.TextBox();
            this.chocolateTb = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.orangeCb = new System.Windows.Forms.CheckBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pancakeCb = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.chocolateCb = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.waterTb = new System.Windows.Forms.TextBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.waterCb = new System.Windows.Forms.CheckBox();
            this.CocaTb = new System.Windows.Forms.TextBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.cocaCb = new System.Windows.Forms.CheckBox();
            this.TeaTb = new System.Windows.Forms.TextBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.TeaCb = new System.Windows.Forms.CheckBox();
            this.label25 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel8 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Grdtotallbl = new System.Windows.Forms.Label();
            this.taxlbl = new System.Windows.Forms.Label();
            this.SubTotallbl = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.ReceiptsTb = new System.Windows.Forms.RichTextBox();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Controls.Add(this.Closelbl);
            this.panel1.Controls.Add(this.Datelbl);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1104, 183);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Closelbl
            // 
            this.Closelbl.AutoSize = true;
            this.Closelbl.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Closelbl.ForeColor = System.Drawing.Color.Khaki;
            this.Closelbl.Location = new System.Drawing.Point(1068, 0);
            this.Closelbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Closelbl.Name = "Closelbl";
            this.Closelbl.Size = new System.Drawing.Size(35, 37);
            this.Closelbl.TabIndex = 0;
            this.Closelbl.Text = "X\r\n";
            this.Closelbl.Click += new System.EventHandler(this.Closelbl_Click);
            // 
            // Datelbl
            // 
            this.Datelbl.AutoSize = true;
            this.Datelbl.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Datelbl.ForeColor = System.Drawing.Color.Khaki;
            this.Datelbl.Location = new System.Drawing.Point(909, 142);
            this.Datelbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Datelbl.Name = "Datelbl";
            this.Datelbl.Size = new System.Drawing.Size(139, 37);
            this.Datelbl.TabIndex = 0;
            this.Datelbl.Text = "Fast Meal\r\n";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(501, 183);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 21);
            this.label17.TabIndex = 0;
            this.label17.Text = "Fast Meal\r\n";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Khaki;
            this.label1.Location = new System.Drawing.Point(287, 51);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(590, 65);
            this.label1.TabIndex = 0;
            this.label1.Text = "FAST FOOD RESTAURANT";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.CheeseTb);
            this.panel2.Controls.Add(this.ChickenTb);
            this.panel2.Controls.Add(this.SandwichTb);
            this.panel2.Controls.Add(this.pictureBox8);
            this.panel2.Controls.Add(this.CheeseCb);
            this.panel2.Controls.Add(this.pictureBox5);
            this.panel2.Controls.Add(this.ChickenCb);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.pictureBox4);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.SandwichCb);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.SaladTb);
            this.panel2.Controls.Add(this.pictureBox3);
            this.panel2.Controls.Add(this.SaladCb);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.BurgerTb);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.BurgerCb);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.FriesTb);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.FriesCb);
            this.panel2.Controls.Add(this.friesecb);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 183);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(251, 566);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // CheeseTb
            // 
            this.CheeseTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.CheeseTb.Location = new System.Drawing.Point(193, 463);
            this.CheeseTb.Margin = new System.Windows.Forms.Padding(4);
            this.CheeseTb.Name = "CheeseTb";
            this.CheeseTb.Size = new System.Drawing.Size(40, 27);
            this.CheeseTb.TabIndex = 14;
            this.CheeseTb.Text = "0";
            this.CheeseTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CheeseTb.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // ChickenTb
            // 
            this.ChickenTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.ChickenTb.Location = new System.Drawing.Point(193, 388);
            this.ChickenTb.Margin = new System.Windows.Forms.Padding(4);
            this.ChickenTb.Name = "ChickenTb";
            this.ChickenTb.Size = new System.Drawing.Size(40, 27);
            this.ChickenTb.TabIndex = 14;
            this.ChickenTb.Text = "0";
            this.ChickenTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ChickenTb.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // SandwichTb
            // 
            this.SandwichTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.SandwichTb.Location = new System.Drawing.Point(193, 313);
            this.SandwichTb.Margin = new System.Windows.Forms.Padding(4);
            this.SandwichTb.Name = "SandwichTb";
            this.SandwichTb.Size = new System.Drawing.Size(40, 27);
            this.SandwichTb.TabIndex = 14;
            this.SandwichTb.Text = "0";
            this.SandwichTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.SandwichTb.TextChanged += new System.EventHandler(this.textBox4_TextChanged_1);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(60, 456);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(37, 43);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 12;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // CheeseCb
            // 
            this.CheeseCb.AutoSize = true;
            this.CheeseCb.Location = new System.Drawing.Point(33, 468);
            this.CheeseCb.Margin = new System.Windows.Forms.Padding(4);
            this.CheeseCb.Name = "CheeseCb";
            this.CheeseCb.Size = new System.Drawing.Size(15, 14);
            this.CheeseCb.TabIndex = 13;
            this.CheeseCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CheeseCb.UseVisualStyleBackColor = true;
            this.CheeseCb.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(60, 376);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(37, 43);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 12;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // ChickenCb
            // 
            this.ChickenCb.AutoSize = true;
            this.ChickenCb.Location = new System.Drawing.Point(33, 392);
            this.ChickenCb.Margin = new System.Windows.Forms.Padding(4);
            this.ChickenCb.Name = "ChickenCb";
            this.ChickenCb.Size = new System.Drawing.Size(15, 14);
            this.ChickenCb.TabIndex = 13;
            this.ChickenCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ChickenCb.UseVisualStyleBackColor = true;
            this.ChickenCb.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(117, 468);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 15);
            this.label11.TabIndex = 11;
            this.label11.Text = "Cheese";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label11.Click += new System.EventHandler(this.label9_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(60, 301);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(37, 43);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(117, 391);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 15);
            this.label8.TabIndex = 11;
            this.label8.Text = "Chicken";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // SandwichCb
            // 
            this.SandwichCb.AutoSize = true;
            this.SandwichCb.Location = new System.Drawing.Point(33, 317);
            this.SandwichCb.Margin = new System.Windows.Forms.Padding(4);
            this.SandwichCb.Name = "SandwichCb";
            this.SandwichCb.Size = new System.Drawing.Size(15, 14);
            this.SandwichCb.TabIndex = 13;
            this.SandwichCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SandwichCb.UseVisualStyleBackColor = true;
            this.SandwichCb.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(117, 316);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 15);
            this.label7.TabIndex = 11;
            this.label7.Text = "Sandwich";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // SaladTb
            // 
            this.SaladTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.SaladTb.Location = new System.Drawing.Point(193, 235);
            this.SaladTb.Margin = new System.Windows.Forms.Padding(4);
            this.SaladTb.Name = "SaladTb";
            this.SaladTb.Size = new System.Drawing.Size(40, 27);
            this.SaladTb.TabIndex = 10;
            this.SaladTb.Text = "0";
            this.SaladTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.SaladTb.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(60, 223);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(37, 43);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // SaladCb
            // 
            this.SaladCb.AutoSize = true;
            this.SaladCb.Location = new System.Drawing.Point(33, 239);
            this.SaladCb.Margin = new System.Windows.Forms.Padding(4);
            this.SaladCb.Name = "SaladCb";
            this.SaladCb.Size = new System.Drawing.Size(15, 14);
            this.SaladCb.TabIndex = 9;
            this.SaladCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SaladCb.UseVisualStyleBackColor = true;
            this.SaladCb.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(117, 237);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 15);
            this.label6.TabIndex = 7;
            this.label6.Text = "Salad";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // BurgerTb
            // 
            this.BurgerTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.BurgerTb.Location = new System.Drawing.Point(193, 155);
            this.BurgerTb.Margin = new System.Windows.Forms.Padding(4);
            this.BurgerTb.Name = "BurgerTb";
            this.BurgerTb.Size = new System.Drawing.Size(40, 27);
            this.BurgerTb.TabIndex = 6;
            this.BurgerTb.Text = "0";
            this.BurgerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.BurgerTb.TextChanged += new System.EventHandler(this.textBox2_TextChanged_1);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(60, 143);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(37, 43);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click_1);
            // 
            // BurgerCb
            // 
            this.BurgerCb.AutoSize = true;
            this.BurgerCb.Location = new System.Drawing.Point(33, 159);
            this.BurgerCb.Margin = new System.Windows.Forms.Padding(4);
            this.BurgerCb.Name = "BurgerCb";
            this.BurgerCb.Size = new System.Drawing.Size(15, 14);
            this.BurgerCb.TabIndex = 5;
            this.BurgerCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.BurgerCb.UseVisualStyleBackColor = true;
            this.BurgerCb.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(117, 157);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "Burger";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label5.Click += new System.EventHandler(this.label5_Click_1);
            // 
            // FriesTb
            // 
            this.FriesTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.FriesTb.Location = new System.Drawing.Point(193, 72);
            this.FriesTb.Margin = new System.Windows.Forms.Padding(4);
            this.FriesTb.Name = "FriesTb";
            this.FriesTb.Size = new System.Drawing.Size(40, 27);
            this.FriesTb.TabIndex = 2;
            this.FriesTb.Text = "0";
            this.FriesTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FriesTb.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(60, 61);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(37, 43);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // FriesCb
            // 
            this.FriesCb.AutoSize = true;
            this.FriesCb.Location = new System.Drawing.Point(32, 72);
            this.FriesCb.Margin = new System.Windows.Forms.Padding(4);
            this.FriesCb.Name = "FriesCb";
            this.FriesCb.Size = new System.Drawing.Size(15, 14);
            this.FriesCb.TabIndex = 1;
            this.FriesCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.FriesCb.UseVisualStyleBackColor = true;
            this.FriesCb.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
            // 
            // friesecb
            // 
            this.friesecb.AutoSize = true;
            this.friesecb.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.friesecb.ForeColor = System.Drawing.Color.Red;
            this.friesecb.Location = new System.Drawing.Point(117, 73);
            this.friesecb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.friesecb.Name = "friesecb";
            this.friesecb.Size = new System.Drawing.Size(33, 15);
            this.friesecb.TabIndex = 0;
            this.friesecb.Text = "Fries";
            this.friesecb.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.friesecb.Click += new System.EventHandler(this.label4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(71, 4);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 30);
            this.label2.TabIndex = 0;
            this.label2.Text = "Fast Meal\r\n";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Khaki;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(26, 566);
            this.panel4.TabIndex = 0;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(846, 183);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(258, 566);
            this.panel3.TabIndex = 2;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.Controls.Add(this.panel9);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(251, 566);
            this.panel6.TabIndex = 2;
            this.panel6.Paint += new System.Windows.Forms.PaintEventHandler(this.panel6_Paint);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.Control;
            this.panel9.Controls.Add(this.orangeTb);
            this.panel9.Controls.Add(this.pancakeTb);
            this.panel9.Controls.Add(this.chocolateTb);
            this.panel9.Controls.Add(this.pictureBox6);
            this.panel9.Controls.Add(this.orangeCb);
            this.panel9.Controls.Add(this.pictureBox7);
            this.panel9.Controls.Add(this.pancakeCb);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.pictureBox9);
            this.panel9.Controls.Add(this.label10);
            this.panel9.Controls.Add(this.chocolateCb);
            this.panel9.Controls.Add(this.label13);
            this.panel9.Controls.Add(this.label15);
            this.panel9.Controls.Add(this.label14);
            this.panel9.Controls.Add(this.label12);
            this.panel9.Controls.Add(this.waterTb);
            this.panel9.Controls.Add(this.pictureBox10);
            this.panel9.Controls.Add(this.waterCb);
            this.panel9.Controls.Add(this.CocaTb);
            this.panel9.Controls.Add(this.pictureBox11);
            this.panel9.Controls.Add(this.cocaCb);
            this.panel9.Controls.Add(this.TeaTb);
            this.panel9.Controls.Add(this.pictureBox12);
            this.panel9.Controls.Add(this.TeaCb);
            this.panel9.Controls.Add(this.label25);
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(251, 566);
            this.panel9.TabIndex = 2;
            this.panel9.Paint += new System.Windows.Forms.PaintEventHandler(this.panel9_Paint);
            // 
            // orangeTb
            // 
            this.orangeTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.orangeTb.Location = new System.Drawing.Point(193, 463);
            this.orangeTb.Margin = new System.Windows.Forms.Padding(4);
            this.orangeTb.Name = "orangeTb";
            this.orangeTb.Size = new System.Drawing.Size(40, 27);
            this.orangeTb.TabIndex = 14;
            this.orangeTb.Text = "0";
            this.orangeTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pancakeTb
            // 
            this.pancakeTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.pancakeTb.Location = new System.Drawing.Point(193, 388);
            this.pancakeTb.Margin = new System.Windows.Forms.Padding(4);
            this.pancakeTb.Name = "pancakeTb";
            this.pancakeTb.Size = new System.Drawing.Size(40, 27);
            this.pancakeTb.TabIndex = 14;
            this.pancakeTb.Text = "0";
            this.pancakeTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chocolateTb
            // 
            this.chocolateTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.chocolateTb.Location = new System.Drawing.Point(193, 313);
            this.chocolateTb.Margin = new System.Windows.Forms.Padding(4);
            this.chocolateTb.Name = "chocolateTb";
            this.chocolateTb.Size = new System.Drawing.Size(40, 27);
            this.chocolateTb.TabIndex = 14;
            this.chocolateTb.Text = "0";
            this.chocolateTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chocolateTb.TextChanged += new System.EventHandler(this.textBox3_TextChanged_1);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(60, 456);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(37, 43);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 12;
            this.pictureBox6.TabStop = false;
            // 
            // orangeCb
            // 
            this.orangeCb.AutoSize = true;
            this.orangeCb.Location = new System.Drawing.Point(33, 468);
            this.orangeCb.Margin = new System.Windows.Forms.Padding(4);
            this.orangeCb.Name = "orangeCb";
            this.orangeCb.Size = new System.Drawing.Size(15, 14);
            this.orangeCb.TabIndex = 13;
            this.orangeCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.orangeCb.UseVisualStyleBackColor = true;
            this.orangeCb.CheckedChanged += new System.EventHandler(this.orangeCb_CheckedChanged);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(60, 376);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(37, 43);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 12;
            this.pictureBox7.TabStop = false;
            // 
            // pancakeCb
            // 
            this.pancakeCb.AutoSize = true;
            this.pancakeCb.Location = new System.Drawing.Point(33, 392);
            this.pancakeCb.Margin = new System.Windows.Forms.Padding(4);
            this.pancakeCb.Name = "pancakeCb";
            this.pancakeCb.Size = new System.Drawing.Size(15, 14);
            this.pancakeCb.TabIndex = 13;
            this.pancakeCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.pancakeCb.UseVisualStyleBackColor = true;
            this.pancakeCb.CheckedChanged += new System.EventHandler(this.pancakeCb_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(117, 468);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 15);
            this.label9.TabIndex = 11;
            this.label9.Text = "orange";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(60, 301);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(37, 43);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 12;
            this.pictureBox9.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(117, 391);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 15);
            this.label10.TabIndex = 11;
            this.label10.Text = "pancake";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // chocolateCb
            // 
            this.chocolateCb.AutoSize = true;
            this.chocolateCb.Location = new System.Drawing.Point(33, 317);
            this.chocolateCb.Margin = new System.Windows.Forms.Padding(4);
            this.chocolateCb.Name = "chocolateCb";
            this.chocolateCb.Size = new System.Drawing.Size(15, 14);
            this.chocolateCb.TabIndex = 13;
            this.chocolateCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chocolateCb.UseVisualStyleBackColor = true;
            this.chocolateCb.CheckedChanged += new System.EventHandler(this.chocolateCb_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(117, 316);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 15);
            this.label13.TabIndex = 11;
            this.label13.Text = "chocolate";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(116, 79);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(26, 15);
            this.label15.TabIndex = 11;
            this.label15.Text = "Tea";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(117, 162);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 15);
            this.label14.TabIndex = 11;
            this.label14.Text = "Coca";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(117, 242);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 15);
            this.label12.TabIndex = 11;
            this.label12.Text = "Water";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // waterTb
            // 
            this.waterTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.waterTb.Location = new System.Drawing.Point(193, 235);
            this.waterTb.Margin = new System.Windows.Forms.Padding(4);
            this.waterTb.Name = "waterTb";
            this.waterTb.Size = new System.Drawing.Size(40, 27);
            this.waterTb.TabIndex = 10;
            this.waterTb.Text = "0";
            this.waterTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.waterTb.TextChanged += new System.EventHandler(this.textBox4_TextChanged_2);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(60, 223);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(37, 43);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 8;
            this.pictureBox10.TabStop = false;
            // 
            // waterCb
            // 
            this.waterCb.AutoSize = true;
            this.waterCb.Location = new System.Drawing.Point(33, 239);
            this.waterCb.Margin = new System.Windows.Forms.Padding(4);
            this.waterCb.Name = "waterCb";
            this.waterCb.Size = new System.Drawing.Size(15, 14);
            this.waterCb.TabIndex = 9;
            this.waterCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.waterCb.UseVisualStyleBackColor = true;
            this.waterCb.CheckedChanged += new System.EventHandler(this.waterCb_CheckedChanged);
            // 
            // CocaTb
            // 
            this.CocaTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.CocaTb.Location = new System.Drawing.Point(193, 155);
            this.CocaTb.Margin = new System.Windows.Forms.Padding(4);
            this.CocaTb.Name = "CocaTb";
            this.CocaTb.Size = new System.Drawing.Size(40, 27);
            this.CocaTb.TabIndex = 6;
            this.CocaTb.Text = "0";
            this.CocaTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CocaTb.TextChanged += new System.EventHandler(this.textBox5_TextChanged_1);
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(60, 143);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(37, 43);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 4;
            this.pictureBox11.TabStop = false;
            // 
            // cocaCb
            // 
            this.cocaCb.AutoSize = true;
            this.cocaCb.Location = new System.Drawing.Point(33, 159);
            this.cocaCb.Margin = new System.Windows.Forms.Padding(4);
            this.cocaCb.Name = "cocaCb";
            this.cocaCb.Size = new System.Drawing.Size(15, 14);
            this.cocaCb.TabIndex = 5;
            this.cocaCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cocaCb.UseVisualStyleBackColor = true;
            this.cocaCb.CheckedChanged += new System.EventHandler(this.cocaCb_CheckedChanged);
            // 
            // TeaTb
            // 
            this.TeaTb.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.TeaTb.Location = new System.Drawing.Point(193, 72);
            this.TeaTb.Margin = new System.Windows.Forms.Padding(4);
            this.TeaTb.Name = "TeaTb";
            this.TeaTb.Size = new System.Drawing.Size(40, 27);
            this.TeaTb.TabIndex = 2;
            this.TeaTb.Text = "0";
            this.TeaTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(60, 61);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(37, 43);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 1;
            this.pictureBox12.TabStop = false;
            // 
            // TeaCb
            // 
            this.TeaCb.AutoSize = true;
            this.TeaCb.Location = new System.Drawing.Point(32, 72);
            this.TeaCb.Margin = new System.Windows.Forms.Padding(4);
            this.TeaCb.Name = "TeaCb";
            this.TeaCb.Size = new System.Drawing.Size(15, 14);
            this.TeaCb.TabIndex = 1;
            this.TeaCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TeaCb.UseVisualStyleBackColor = true;
            this.TeaCb.CheckedChanged += new System.EventHandler(this.TeaCb_CheckedChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(71, 4);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(112, 30);
            this.label25.TabIndex = 0;
            this.label25.Text = "Fast Meal\r\n";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Khaki;
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(26, 566);
            this.panel10.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(54, 0);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(153, 30);
            this.label16.TabIndex = 0;
            this.label16.Text = "Drink Dessert";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(4, 4);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "Drinks And Dessert";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Khaki;
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(228, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(30, 566);
            this.panel5.TabIndex = 0;
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Khaki;
            this.panel7.Controls.Add(this.richTextBox1);
            this.panel7.Controls.Add(this.label18);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.ForeColor = System.Drawing.Color.Khaki;
            this.panel7.Location = new System.Drawing.Point(251, 183);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(595, 51);
            this.panel7.TabIndex = 3;
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(26, 113);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(306, 293);
            this.richTextBox1.TabIndex = 5;
            this.richTextBox1.Text = "";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(251, 4);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(147, 40);
            this.label18.TabIndex = 0;
            this.label18.Text = "Fast Meal\r\n";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.button3);
            this.panel8.Controls.Add(this.button2);
            this.panel8.Controls.Add(this.Grdtotallbl);
            this.panel8.Controls.Add(this.taxlbl);
            this.panel8.Controls.Add(this.SubTotallbl);
            this.panel8.Controls.Add(this.button1);
            this.panel8.Controls.Add(this.label22);
            this.panel8.Controls.Add(this.label20);
            this.panel8.Controls.Add(this.label19);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(251, 651);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(595, 98);
            this.panel8.TabIndex = 4;
            this.panel8.Paint += new System.Windows.Forms.PaintEventHandler(this.panel8_Paint);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Red;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button3.ForeColor = System.Drawing.Color.Khaki;
            this.button3.Location = new System.Drawing.Point(439, 39);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(63, 33);
            this.button3.TabIndex = 9;
            this.button3.Text = "PRINT";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button2.ForeColor = System.Drawing.Color.Khaki;
            this.button2.Location = new System.Drawing.Point(254, 39);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 33);
            this.button2.TabIndex = 8;
            this.button2.Text = "ADD";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Grdtotallbl
            // 
            this.Grdtotallbl.AutoSize = true;
            this.Grdtotallbl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Grdtotallbl.ForeColor = System.Drawing.Color.Black;
            this.Grdtotallbl.Location = new System.Drawing.Point(481, 21);
            this.Grdtotallbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Grdtotallbl.Name = "Grdtotallbl";
            this.Grdtotallbl.Size = new System.Drawing.Size(35, 15);
            this.Grdtotallbl.TabIndex = 7;
            this.Grdtotallbl.Text = "Rs/--";
            this.Grdtotallbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // taxlbl
            // 
            this.taxlbl.AutoSize = true;
            this.taxlbl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.taxlbl.ForeColor = System.Drawing.Color.Black;
            this.taxlbl.Location = new System.Drawing.Point(288, 21);
            this.taxlbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.taxlbl.Name = "taxlbl";
            this.taxlbl.Size = new System.Drawing.Size(35, 15);
            this.taxlbl.TabIndex = 6;
            this.taxlbl.Text = "Rs/--";
            this.taxlbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SubTotallbl
            // 
            this.SubTotallbl.AutoSize = true;
            this.SubTotallbl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.SubTotallbl.ForeColor = System.Drawing.Color.Black;
            this.SubTotallbl.Location = new System.Drawing.Point(99, 21);
            this.SubTotallbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SubTotallbl.Name = "SubTotallbl";
            this.SubTotallbl.Size = new System.Drawing.Size(35, 15);
            this.SubTotallbl.TabIndex = 5;
            this.SubTotallbl.Text = "Rs/--";
            this.SubTotallbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.SubTotallbl.Click += new System.EventHandler(this.label21_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button1.ForeColor = System.Drawing.Color.Khaki;
            this.button1.Location = new System.Drawing.Point(36, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 33);
            this.button1.TabIndex = 4;
            this.button1.Text = "RESET";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(439, 21);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(34, 15);
            this.label22.TabIndex = 3;
            this.label22.Text = "Total";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(254, 21);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(26, 15);
            this.label20.TabIndex = 1;
            this.label20.Text = "Tax";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(36, 21);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 15);
            this.label19.TabIndex = 0;
            this.label19.Text = "SubTotal\r\n";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label19.Click += new System.EventHandler(this.label4_Click);
            // 
            // ReceiptsTb
            // 
            this.ReceiptsTb.Location = new System.Drawing.Point(251, 234);
            this.ReceiptsTb.Name = "ReceiptsTb";
            this.ReceiptsTb.Size = new System.Drawing.Size(595, 431);
            this.ReceiptsTb.TabIndex = 5;
            this.ReceiptsTb.Text = " ";
            this.ReceiptsTb.TextChanged += new System.EventHandler(this.ReceiptsTb_TextChanged);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 749);
            this.Controls.Add(this.ReceiptsTb);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private Panel panel2;
        private Panel panel3;
        private Label label1;
        private Panel panel4;
        private Panel panel5;
        private Label label2;
        private Label label3;
        private PictureBox pictureBox1;
        private CheckBox FriesCb;
        private Label friesecb;
        private TextBox FriesTb;
        private TextBox CheeseTb;
        private TextBox ChickenTb;
        private TextBox SandwichTb;
        private PictureBox pictureBox5;
        private CheckBox ChickenCb;
        private PictureBox pictureBox4;
        private Label label8;
        private CheckBox SandwichCb;
        private Label label7;
        private TextBox SaladTb;
        private PictureBox pictureBox3;
        private CheckBox SaladCb;
        private Label label6;
        private TextBox BurgerTb;
        private PictureBox pictureBox2;
        private CheckBox BurgerCb;
        private Label label5;
        private PictureBox pictureBox8;
        private CheckBox CheeseCb;
        private Label label11;
        private Panel panel6;
        private Label label16;
        private Panel panel7;
        private Label Datelbl;
        private Label label17;
        private Label label18;
        private System.Windows.Forms.Timer timer1;
        private Label Closelbl;
        private Panel panel8;
        private Label label19;
        private Label label22;
        private Label label20;
        private Button button1;
        private Label Grdtotallbl;
        private Label taxlbl;
        private Label SubTotallbl;
        private Button button3;
        private Button button2;
        private RichTextBox richTextBox1;
        private RichTextBox ReceiptsTb;
        private Panel panel9;
           private Label label25;
        private TextBox orangeTb;
        private TextBox pancakeTb;
        private TextBox chocolateTb;
        private PictureBox pictureBox6;
        private CheckBox orangeCb;
        private PictureBox pictureBox7;
        private CheckBox pancakeCb;
        private Label label9;
        private PictureBox pictureBox9;
        private Label label10;
        private CheckBox chocolateCb;
        private Label label12;
        private TextBox waterTb;
        private PictureBox pictureBox10;
        private CheckBox waterCb;
        private TextBox CocaTb;
        private PictureBox pictureBox11;
        private CheckBox cocaCb;
        private PictureBox pictureBox12;
        private CheckBox TeaCb;
        private Panel panel10;
        private Label label13;
        private Label label15;
        private Label label14;
        private TextBox TeaTb;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private PrintPreviewDialog printPreviewDialog1;
    }
}